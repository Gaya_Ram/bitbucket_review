"""
This is a small mini app.
"""

from flask import Flask, request, g
from flask_restful import abort, Resource, Api
import psycopg2
from peewee import *
from playhouse.shortcuts import model_to_dict
import logging
import os

# CONFIG
DB_NAME = 'bitbucket-review-db'
DB_HOST = '127.0.0.1'
DB_PORT = '5432'

# Connect to local database
psql_db = PostgresqlDatabase(
    database=DB_NAME,
    host=DB_HOST,
    port=DB_PORT
)


# Start application
application = Flask(__name__)
api = Api(application)

# Database models
class BaseModel(Model):
    class Meta:
        database = psql_db

class User(BaseModel):
    username = CharField()
    password = CharField()

# Connect to database and create tables (if they do not already exist)
psql_db.connect()
psql_db.create_tables([User])

# GET & POST /
class APIUsers(Resource):
    """
    Use to post a new user.
    """

    @staticmethod
    def post():
        """
        Post a new user
        """
        User.create(
            username=request.json['username'],
            password=request.json['password']
        )
        return 'Complete'

# GET, PUT, DELETE /<id>
class APIUser(Resource):

    @staticmethod
    def get(user_id):
        """
        User ID
        """
        temp_user = User.get(User.id == user_id)
        temp_user = model_to_dict(temp_user)
        return {
            "id": user_id,
            "username": temp_user["username"],
            "password": temp_user["password"]
        }


# Routes
api.add_resource(APIUsers, '/')
api.add_resource(APIUser, '/<int:user_id>')

if __name__ == '__main__':
    application.run(debug=True)
